# Desafio Pessoa Desenvolvedora .NET

Oi, sou o **Bruno Leone** 😎

Estou muito feliz por ter recebido uma chance de avaliação na **IOASYS**, e já me sinto imensamente satisfeito por estar em contato com pessoas incríveis como vocês!

Encarei esse como um real desafio.
Mantive meu padrão de desenvolvimento, mesmo tendo em vista que o tempo não era ao meu favor. Encontrei dificuldades, aprendi com elas e pretendo dar andamento mesmo depois de entregue.

Adianto aqui que o projeto está cheio de "TODOs", mas espero que levem em consideração o tempo disposto para a execução das tarefas e a rotina de um programador dono de casa e filho da **Safira** 🐶.

Agora chega de enrolação, rode o comando `git checkout origin/development` para ver meu código.

Aguardo feedback 🤙